import './App.css';
import Burger from "./Components/Burger/Burger";

function App() {
    return (
        <div className="App">
            <Burger/>
        </div>
    );
}

export default App;
