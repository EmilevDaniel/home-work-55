import React from 'react';
import {nanoid} from "nanoid";


const IngredientsList = props => {
    const menu = props.burger.map((e) => (
        <div style={{display: 'flex'}} key={e.id}>
            <button onClick={() => props.addIngredient(e.name)}>Add</button>
            <div>{e.name}</div>
            <div>{e.count}</div>
            <button onClick={()=> props.removeIngredient(e.name)}>Delete</button>
        </div>
    ),);

    return (
        <div>
            {menu}
        </div>
    );
};

export default IngredientsList;