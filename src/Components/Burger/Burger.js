import React from 'react';
import {useState} from "react";
import './Burger.css'
import IngredientsList from "../IngredientsList/IngredientsList";
import {nanoid} from "nanoid";

const Burger = () => {
    const [burger, setBurger] = useState([
        {name: 'Meat', count: 0, price: 50, id: nanoid()},
        {name: 'Cheese', count: 0, price: 20, id: nanoid()},
        {name: 'Salad', count: 0, price: 5, id: nanoid()},
        {name: 'Bacon', count: 0, price: 30, id: nanoid()},
    ]);

    const addIngredient = name => {
        setBurger(burger.map(p => {
            if (name === p.name) {
                return {...p, count: p.count + 1}
            }
            return p;
        }))
    };

    const removeIngredient = name => {
        setBurger(burger.map(p => {
            if (name === p.name && p.count > 0) {
                return {...p, count: p.count - 1}
            }
            return p;
        }))
    };


    const myBurger = [];
    burger.map(e => {
            if (e.count > 0) {
                for (let i = 0; i < e.count; i++) {
                    myBurger.push(<div className={e.name} style={{width: '150px', height: '10px'}} key={nanoid()}/>)
                }
            }
        }
    );

    const totalPrice = burger.map(e => {
        return e.count * e.price
    }).reduce((acc, cur) => acc + cur, 0);

    return (
        <div>
            <IngredientsList addIngredient={addIngredient} removeIngredient={removeIngredient} burger={burger}/>
            <div>
                <div className='BreadTop' style={{width: '150px', height: '50px'}}></div>
                {myBurger}
                <div className='BreadBottom' style={{width: '150px', height: '50px'}}></div>
            </div>
            <div>Total price: {totalPrice}</div>
        </div>
    );
};

export default Burger;